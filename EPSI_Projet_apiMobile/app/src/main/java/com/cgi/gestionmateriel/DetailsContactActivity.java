package com.cgi.gestionmateriel;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class DetailsContactActivity extends AppCompatActivity {

    int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_contact);

        Integer TempHolder = getIntent().getIntExtra("clickPositionValue", 0);

        position = TempHolder;

        new DetailsContactActivity.ParseTask().execute();
    }

    private class ParseTask extends AsyncTask<Void, Void, String> {
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        String resultJson = "";

        @Override
        protected String doInBackground(Void... params) {
            try {
//                String url_json = "https://api.androidhive.info/contacts/";
                String url_json = "http://formation.devatom.net/UDEV2/ProjetFilRouge/JSON/exploded/netgest_personne.json";
                URL url = new URL(url_json);

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();

                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }

                resultJson = buffer.toString();
                Log.d("FOR_LOG", resultJson);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultJson;
        }

        protected void onPostExecute(String strJson) {
            super.onPostExecute(strJson);

            final EditText ETname    = (EditText) findViewById(R.id.TextContactName);
            final EditText ETprename = (EditText) findViewById(R.id.TextContactPrenom);
            final EditText ETmail    = (EditText) findViewById(R.id.TextContactMail);
            final EditText ETphone   = (EditText) findViewById(R.id.TextContactPhone);

            try {
                JSONArray jArray = new JSONArray(strJson);

                    JSONObject friend = jArray.getJSONObject(position);

                    String name = friend.getString("nom");
                    String prenom = friend.getString("prenom");
                    String tel = friend.getString("telephone");
                    String email = friend.getString("email");

                    Log.d("FOR_LOG", name);
                    Log.d("FOR_LOG", prenom);
                    Log.d("FOR_LOG", tel);
                    Log.d("FOR_LOG", email);

                    ETname.setText(name);
                    ETprename.setText(prenom);
                    ETmail.setText(email);
                    ETphone.setText(tel);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
