package com.cgi.gestionmateriel;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void goToCreateHardware(View view) {
        Intent intent = new Intent(MainActivity.this, HardwareActivity.class);
        startActivity(intent);
    }

    public void goToClients(View view) {
        Intent intent = new Intent(MainActivity.this, ClientsActivity.class);
        startActivity(intent);
    }

    public void goToContact(View view) {
        Intent intent = new Intent(MainActivity.this, ContactActivity.class);
        startActivity(intent);
    }

    public void goToQrScan(View view) {
        Intent intent = new Intent(MainActivity.this, QrScanActivity.class);
        startActivity(intent);
    }
}
