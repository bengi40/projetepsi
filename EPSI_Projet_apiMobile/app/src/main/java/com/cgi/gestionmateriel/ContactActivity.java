package com.cgi.gestionmateriel;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class ContactActivity extends AppCompatActivity {

    private TextView TitrePageContact;
    private Button CreerContact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        new ContactActivity.ParseTask().execute();

        TitrePageContact = (TextView) findViewById(R.id.TitrePageContact);
        CreerContact = (Button) findViewById(R.id.CreerContact);
    }

    public void goToCreateContact(View view) {
        Intent intent = new Intent(ContactActivity.this, CreateContactActivity.class);
        startActivity(intent);
    }

    private class ParseTask extends AsyncTask<Void, Void, String> {
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        String resultJson = "";

        @Override
        protected String doInBackground(Void... params) {
            try {
//                String url_json = "https://api.androidhive.info/contacts/";
                String url_json = "http://formation.devatom.net/UDEV2/ProjetFilRouge/JSON/exploded/netgest_personne.json";
                URL url = new URL (url_json);

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();

                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }

                resultJson = buffer.toString();
                Log.d("FOR_LOG", resultJson);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return  resultJson;
        }

        protected void  onPostExecute(String strJson) {
            super.onPostExecute(strJson);

            final ListView listView = (ListView) findViewById(R.id.ListContact);

            String[] from = {"name_item"};
            int[] to = {R.id.name_item};
            ArrayList<HashMap<String, String>> arrayList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> hashmap;

            try {
//                JSONObject json = new JSONObject(strJson);
//                JSONArray jArray = json.getJSONArray("");
//                JSONArray jArray = json.getJSONArray(strJson);
                JSONArray jArray = new JSONArray(strJson);

                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject friend = jArray.getJSONObject(i);

                    String nom = friend.getString("nom");
                    String prenom = friend.getString("prenom");
//                    String name = friend.getString("name");
                    Log.d("FOR_LOG", nom);
                    Log.d("FOR_LOG", prenom);

                    hashmap = new HashMap<String, String>();
                    hashmap.put("name_item", "" + nom + " " + prenom );
                    arrayList.add(hashmap);
                }

                final SimpleAdapter adapter = new SimpleAdapter(ContactActivity.this, arrayList, R.layout.items, from, to);
                listView.setAdapter(adapter);
            } catch (JSONException e) {
                e.printStackTrace();
            }
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {


                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                        Toast.makeText(ClientsActivity.this, "Vous avez cliqué a la position" + position + id, Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getApplicationContext(), DetailsContactActivity.class);

                        intent.putExtra("clickPositionValue", position);
//                        intent.putExtra("name",from[position]);
                        startActivity(intent);
                    }
                });

        }
    }
}
