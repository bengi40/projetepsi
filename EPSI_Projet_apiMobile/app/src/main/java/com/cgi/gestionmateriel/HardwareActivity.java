package com.cgi.gestionmateriel;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class HardwareActivity extends Activity {

    private TextView HardwarePageTitle;
    private Button AddHardware;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager
            layoutManager = new LinearLayoutManager(this);

    protected ArrayList<Hardware> arrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hardware);
        recyclerView = (RecyclerView) findViewById(R.id.hardwareRecyclerView);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);

        new ParseTask().execute();

        HardwarePageTitle = (TextView) findViewById(R.id.hardwarePageTitle);
        AddHardware = (Button) findViewById(R.id.addHardware);

        mAdapter = new HardwareAdapter(arrayList);
        recyclerView.setAdapter(mAdapter);
    }

    public void goToCreateHardware(View view) {
        Intent intent = new Intent(HardwareActivity.this, CreateHardwareActivity.class);
        startActivity(intent);
    }

    private class ParseTask extends AsyncTask<Void, Void, String[]>{
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;

        @Override
        protected String[] doInBackground(Void... params) {

            String resultJsonMaterial = getJsonFromUrl("https://pastebin.com/raw/sYrxKXgQ");
            String resultJsonTypes = getJsonFromUrl("https://pastebin.com/raw/PfVBsH3G");

            String[] arrstr = { resultJsonMaterial, resultJsonTypes };
            return  arrstr;
        }

        private String getJsonFromUrl(String url_json) {
            String resultJson = "";
            try {
                URL url = new URL (url_json);

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();

                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }

                resultJson = buffer.toString();
                Log.d("FOR_LOG", resultJson);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultJson;
        }

        protected void onPostExecute(String[] strJsons) {
            super.onPostExecute(strJsons);

            String[] from = {"activity_hardware_line"};
            int[] to = {R.id.hardwareName};

            try {

                Map<Integer, String> typeMap = new HashMap<>();

                JSONArray jArrayType = new JSONArray(strJsons[1]);
                for (int i = 0; i < jArrayType.length(); i++) {
                    JSONObject typeJson = jArrayType.getJSONObject(i);
                    int id = typeJson.getInt("id");
                    String name = typeJson.getString("libelle");
                    typeMap.put(id, name);
                }

                JSONArray jArrayHardware = new JSONArray(strJsons[0]);

                for (int i = 0; i < jArrayHardware.length(); i++) {
                    JSONObject hardwareJson = jArrayHardware.getJSONObject(i);

                    int id = hardwareJson.getInt("id");
                    String name = hardwareJson.getString("libelle");
                    int type_id = hardwareJson.getInt("idtype");
                    int client_id = hardwareJson.getInt("idclient");

                    Hardware hardware = new Hardware(id, name, type_id, client_id);
                    String typeName = typeMap.get(type_id);
                    hardware.setType_name(typeName);

                    Log.d("FOR_LOG", name);

                    arrayList.add(hardware);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            mAdapter.notifyDataSetChanged();
        }
    }
}
