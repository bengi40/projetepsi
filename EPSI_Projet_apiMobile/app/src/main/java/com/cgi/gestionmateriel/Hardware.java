package com.cgi.gestionmateriel;

public class Hardware {

    private int id;
    private String name;
    private int type_id;
    private String type_name = "";
    private int client_id;
    private String client_name = "";

    public Hardware(int id, String name, int type_id, int client_id) {
        this.id = id;
        this.name = name;
        this.type_id = type_id;
        this.client_id = client_id;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getType_id() {
        return type_id;
    }

    public String getType_name() {
        return type_name;
    }

    public int getClient_id() {
        return client_id;
    }

    public String getClient_name() {
        return client_name;
    }

    public void setType_name(String type_name) {
        this.type_name = type_name;
    }

    public void setClient_name(String client_name) {
        this.client_name = client_name;
    }
}
