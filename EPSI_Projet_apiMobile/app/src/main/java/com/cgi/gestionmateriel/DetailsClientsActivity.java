package com.cgi.gestionmateriel;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.SimpleAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

public class DetailsClientsActivity extends AppCompatActivity {

    int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_clients);

        Integer TempHolder = getIntent().getIntExtra("clickPositionValue", 0);

        position = TempHolder;

        new DetailsClientsActivity.ParseTask().execute();

    }


    private class ParseTask extends AsyncTask<Void, Void, String> {
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        String resultJson = "";

        @Override
        protected String doInBackground(Void... params) {
            try {
//                String url_json = "https://api.androidhive.info/contacts/";
                String url_json = "http://formation.devatom.net/UDEV2/ProjetFilRouge/JSON/combined/clients_contacts.json";
                URL url = new URL(url_json);

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();

                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }

                resultJson = buffer.toString();
                Log.d("FOR_LOG", resultJson);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultJson;
        }

        protected void onPostExecute(String strJson) {
            super.onPostExecute(strJson);

            final EditText ETname = (EditText) findViewById(R.id.TextClientName);
            final EditText ETadress = (EditText) findViewById(R.id.TextClientAdresse);
            final EditText ETcity = (EditText) findViewById(R.id.TextClientVille);
            final EditText ETcodePo = (EditText) findViewById(R.id.TextClientCP);

            try {
                JSONArray jArray = new JSONArray(strJson);
//                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject friend = jArray.getJSONObject(position);

                    JSONObject ville = friend.getJSONObject("ville");

                    String name = friend.getString("clt_nom");
                    String adress = friend.getString("clt_adr1");
                    String city = ville.getString("ville_nom");
                    String codePo = ville.getString("ville_cp");

                    Log.d("FOR_LOG", name);
                    Log.d("FOR_LOG", adress);
                    Log.d("FOR_LOG", city);
                    Log.d("FOR_LOG", codePo);

                    ETname.setText(name);
                    ETadress.setText(adress);
                    ETcity.setText(city);
                    ETcodePo.setText(codePo);

                } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}