package com.cgi.udev.resoapi.web.activitylayer;

import java.sql.SQLException;

import com.cgi.udev.resoapi.dao.CAdresseIpDao;
import com.cgi.udev.resoapi.web.model.CAdresseIpModel;

public class SCAdresseIpActivityLayer {
	public static CAdresseIpModel getAdresseById(int id) throws SQLException {
		CAdresseIpDao oGetAdresseByIdDao = new CAdresseIpDao();
		return oGetAdresseByIdDao.getAdresseById(id);
	}

	public static void createAdresseIp(CAdresseIpModel newAdresseIp) {
		CAdresseIpDao oCreateAdresseIp = new CAdresseIpDao();
		oCreateAdresseIp.createAdresseIp(newAdresseIp);
		
	}

	public static void updateAdresseIp(CAdresseIpModel majAdresseIp) throws SQLException {
		CAdresseIpDao oUpdateAdresseIp = new CAdresseIpDao();
		oUpdateAdresseIp.updateAdresseIp(majAdresseIp);
		
	}

	public static void deleteAdresseIp(int id) throws SQLException {
		CAdresseIpDao oDeleteAdresseIp = new CAdresseIpDao();
		oDeleteAdresseIp.deleteAdresseIp(id);
	}
}
