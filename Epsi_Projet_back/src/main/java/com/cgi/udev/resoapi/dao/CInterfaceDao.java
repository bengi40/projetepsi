package com.cgi.udev.resoapi.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.cgi.udev.resoapi.web.model.CAdresseIpModel;
import com.cgi.udev.resoapi.web.model.CInterfaceModel;

public class CInterfaceDao {

	public CInterfaceModel getInterfaceById(int id) throws SQLException {
		
		String request = "select  " + 
						"	i.id iid, " + 
						"	i.nom inom, " + 
						"	i.mac imac, " + 
						"	i.isActived iactiv, " +
						"	a.id aid, " +
						"	a.ipv4 v4, " + 
						"	a.ipv6 v6, " + 
						"	a.masque masque," +
						"	a.isActived aactiv, " + 
						"	ta.libelle libelle " + 
						"from " + 
						"	interface i " + 
						"left outer join adresseip a on a.id = i.id " + 
						"left outer join typeaffectation ta on ta.id = a.id " +
						"WHERE i.id = ?;";
				
		try (Connection c = ResoDataSource.getSingleton().getConnection();
			PreparedStatement pstmt = c.prepareStatement(request)) {
			
			pstmt.setInt(1, id);
				
			ResultSet rs = pstmt.executeQuery();
			
			List<CAdresseIpModel> vAdresseIp = new ArrayList<>();
			CInterfaceModel oReturnedValue = null;
			
			try {
				if (rs.next()) {
					oReturnedValue = new CInterfaceModel(
							rs.getInt("iid"),
							rs.getString("inom"),
							rs.getString("imac"),
							rs.getBoolean("iactiv"),
							vAdresseIp
							);
					do {
						if (rs.getInt("iid") != 0) {
							vAdresseIp.add(new CAdresseIpModel
									(
										rs.getInt("aid"),
										rs.getString("v4"),
										rs.getString("v6"),
										rs.getString("masque"),
										rs.getBoolean("aactiv"),
										rs.getString("libelle")
									));
						}
					} while (rs.next());
				}
			} finally {
				
			}
			return oReturnedValue;
		}
	}
	
	public void createInterface(CInterfaceModel newInterface) {
		String request = "INSERT INTO "
						+ "		interface ( nom, mac, isActived, idtype, idmateriel) "
						+ " VALUES "
						+ "		(?,?,?,?,?);";
		
		try (Connection c = ResoDataSource.getSingleton().getConnection();
				PreparedStatement pstmt = c.prepareStatement(request)) {
				
				pstmt.setString(1, newInterface.getMsNom());
				pstmt.setString(2, newInterface.getMsMac());
				pstmt.setBoolean(3, newInterface.isMbIsactivd());
				pstmt.setInt(4, newInterface.getMiIdType());
				pstmt.setInt(5, newInterface.getMiIdMateriel());
							
				pstmt.executeUpdate();
				
				c.close();

			}catch (SQLException e) {
				e.printStackTrace();
			}
	}

	public void updateInterface(CInterfaceModel majInterface) {
		String request = ("UPDATE interface " +
							"SET " + 
							"	nom = ?," + 
							"	mac = ?," + 
							"	isActived = ?, " + 
							"	idtype = ?, " + 
							"	idmateriel = ? " + 
							"WHERE id = ?;" );
		try (Connection c = ResoDataSource.getSingleton().getConnection();
				PreparedStatement pstmt = c.prepareStatement(request)) {
			pstmt.setString(1, majInterface.getMsNom());
			pstmt.setString(2, majInterface.getMsMac());
			pstmt.setBoolean(3, majInterface.isMbIsactivd());
			pstmt.setInt(4, majInterface.getMiIdType());
			pstmt.setInt(5, majInterface.getMiIdMateriel()); 
			pstmt.setInt(6, majInterface.getMiId());
			
			pstmt.executeUpdate();
			
			c.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}		
	}
	
	public void deleteInterface(CInterfaceModel deleteInterface) {
		String request = ("UPDATE interface " +
							"SET " + 
							"	isActived = ? " +  
							"WHERE id = ?;" );
		try (Connection c = ResoDataSource.getSingleton().getConnection();
				PreparedStatement pstmt = c.prepareStatement(request)) {
			pstmt.setBoolean(1, deleteInterface.isMbIsactivd());
			pstmt.setInt(2, deleteInterface.getMiId());
			
			pstmt.executeUpdate();
			
			c.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}		
	}
}
