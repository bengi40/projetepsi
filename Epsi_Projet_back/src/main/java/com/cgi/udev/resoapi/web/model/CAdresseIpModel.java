package com.cgi.udev.resoapi.web.model;

public class CAdresseIpModel {

	private int miId;
	private String msIpv4;
	private String msIpv6;
	private String msMasque;
	private boolean mbIsActived;
	private int miIdType;
	private int miInterfaceId;
	
	private String msTypeAffectationLibelle;
	
	public CAdresseIpModel() {
		this.miId = 0;
		this.msIpv4 = "";
		this.msIpv6 = "";
		this.msMasque = "";
		this.mbIsActived = true;
		this.miInterfaceId = 0;
		this.msTypeAffectationLibelle = "";
	}
	
	public CAdresseIpModel(int piId, String psIpv4, String psIpv6, String psMasque, boolean pvIsActived,String psTypeAffectationLibelle) {
		this.miId = piId;
		this.msIpv4 = psIpv4;
		this.msIpv6 = psIpv6;
		this.msMasque = psMasque;
		this.mbIsActived = pvIsActived;
		this.msTypeAffectationLibelle = psTypeAffectationLibelle;
	}
	
	public CAdresseIpModel(int piId, String psIpv4, String psIpv6, String psMasque, boolean pvIsActived, int piIdType, String psTypeAffectationLibelle, int piInterfaceId) {
		this.miId = piId;
		this.msIpv4 = psIpv4;
		this.msIpv6 = psIpv6;
		this.msMasque = psMasque;
		this.mbIsActived = pvIsActived;
		this.miIdType = piIdType;
		this.msTypeAffectationLibelle = psTypeAffectationLibelle;
		this.miInterfaceId = piInterfaceId;
	}

	public int getMiId() {
		return miId;
	}

	public void setMiId(int miId) {
		this.miId = miId;
	}

	public String getMsIpv4() {
		return msIpv4;
	}

	public void setMsIpv4(String msIpv4) {
		this.msIpv4 = msIpv4;
	}

	public String getMsIpv6() {
		return msIpv6;
	}

	public void setMsIpv6(String msIpv6) {
		this.msIpv6 = msIpv6;
	}

	public String getMsMasque() {
		return msMasque;
	}

	public void setMsMasque(String msMasque) {
		this.msMasque = msMasque;
	}

	public boolean isMbIsActived() {
		return mbIsActived;
	}

	public void setMbIsActived(boolean mbIsActived) {
		this.mbIsActived = mbIsActived;
	}

	public String getMsTypeAffectationLibelle() {
		return msTypeAffectationLibelle;
	}

	public void setMsTypeAffectationLibelle(String msTypeAffectationLibelle) {
		this.msTypeAffectationLibelle = msTypeAffectationLibelle;
	}

	public int getMiIdType() {
		return miIdType;
	}

	public void setMiIdType(int miIdType) {
		this.miIdType = miIdType;
	}

	public int getMiInterfaceId() {
		return miInterfaceId;
	}

	public void setMiInterfaceId(int miInterfaceId) {
		this.miInterfaceId = miInterfaceId;
	}
	
}
