package com.cgi.udev.resoapi.web.restcontroller;

import java.sql.SQLException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.cgi.udev.resoapi.web.activitylayer.SCClientActivityLayer;
import com.cgi.udev.resoapi.web.model.CClientModel;

@Path("/client")
public class CClientRestFullService {

	/**
	 * Service REST Client
	 * @throws SQLException
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<CClientModel> getClient() throws SQLException {
		return SCClientActivityLayer.getClient();
	}
	
	/**
	 * Service REST Detail client
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public CClientModel getClientByID(@PathParam("id") Integer id) throws SQLException {
		return SCClientActivityLayer.getClientById(id);
	}
	
	/**
	 * Création d'un client
	 * @param id
	 * @param nom
	 * @param adresse
	 * @param adresse2
	 * @param idCpVille
	 * @throws SQLException
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public void createClient(CClientModel newClient) throws SQLException {
		SCClientActivityLayer.createClient(newClient);
	}
	
	/**
	 * Mise à jour du client
	 * @param majClient
	 * @throws SQLException
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public void updateClient(CClientModel majClient) throws SQLException {
		SCClientActivityLayer.updateClient(majClient);
	}
		
	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public void delClientById(CClientModel delClient) throws SQLException {
		SCClientActivityLayer.delClientById(delClient);
	}
}
