package com.cgi.udev.resoapi.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.cgi.udev.resoapi.web.model.CContactModel;
import com.cgi.udev.resoapi.web.model.CMaterielModel;

public class CMaterielDao {

	public CMaterielModel getMaterielById(Integer id) throws SQLException {
		String request ="select " + 
						"	m.id mid, " + 
						"	m.libelle mlib," + 
						"	m.numserie mnum," + 
						"	m.isActived mactiv," + 
						"	t.id tid, " + 
						"	t.libelle tlib " + 
						"from materiel m " + 
						"inner join typemateriel t on t.id = m.id " +
						"WHERE m.id = ? ;";

		try (Connection c = ResoDataSource.getSingleton().getConnection();
				PreparedStatement pstmt = c.prepareStatement(request)) {
			pstmt.setInt(1, id);	
			
			ResultSet rs = pstmt.executeQuery();
			CMaterielModel oReturnedValue = null;
			try {
				if(rs.next()) {
					oReturnedValue = new CMaterielModel(
							rs.getInt("mid"), 
							rs.getString("mlib"), 
							rs.getString("mnum"),
							rs.getBoolean("mactiv"),
							rs.getInt("tid"),
							rs.getString("tlib"));
				}
				return oReturnedValue;
			} finally {
				
			}
		}
		
	}

	public void createMateriel(CMaterielModel newMateriel) throws SQLException {
		String request = "INSERT INTO materiel "
							+ "	(libelle, "
							+ "numserie,"
							+ "isActived, "
							+ "idclient,"
							+ "idtype) " 
						+ "VALUES "
						+ "	(?,?,?,?,?);";
		
		try (Connection c = ResoDataSource.getSingleton().getConnection();
				PreparedStatement pstmt = c.prepareStatement(request)) {
			
			pstmt.setString(1,newMateriel.getMsLibelle());	
			pstmt.setString(2, newMateriel.getMsNumSerie());
			pstmt.setBoolean(3, newMateriel.isMbIsActived());
			pstmt.setInt(4,newMateriel.getMiIdClient());
			pstmt.setInt(5, newMateriel.getMiIdTypeMateriel());
			
			pstmt.executeUpdate();
			c.close();
			
		}catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void updateMateriel(CMaterielModel updateMateriel) throws SQLException {
		String request = "update materiel " + 
							"set	" + 
							"	libelle = ?, " + 
							"	isActived = ? " + 
							"where " + 
							"	id = ?;";
		
		try (Connection c = ResoDataSource.getSingleton().getConnection();
				PreparedStatement pstmt = c.prepareStatement(request)) {
			
			pstmt.setString(1, updateMateriel.getMsLibelle());
			pstmt.setBoolean(2, updateMateriel.isMbIsActived());
			pstmt.setInt(3, updateMateriel.getMiId());
			
		}
		
	}

	public void deleteMaterielByID(CContactModel deleteMaterielByID) throws SQLException {
		String request = "update materiel " + 
						"set	" + 
						"	isActived = ? " + 
						"where " + 
						"	id = ?;";
		try (Connection c = ResoDataSource.getSingleton().getConnection();
				PreparedStatement pstmt = c.prepareStatement(request)) {
			
			pstmt.setBoolean(1, deleteMaterielByID.isMbIsActived());
			pstmt.setInt(2, deleteMaterielByID.getMiId());
			
		}
	}

}
