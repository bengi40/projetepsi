package com.cgi.udev.resoapi.web.restcontroller;

import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.cgi.udev.resoapi.web.activitylayer.SCMaterielActivityLayer;
import com.cgi.udev.resoapi.web.model.CContactModel;
import com.cgi.udev.resoapi.web.model.CMaterielModel;

@Path("/materiel")
public class CMaterielRestFullService {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public CMaterielModel getMaterielById(@PathParam("id") Integer id) throws SQLException {
		return SCMaterielActivityLayer.getMaterielById(id);
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public void createMateriel(CMaterielModel newMateriel) throws SQLException {
		SCMaterielActivityLayer.createMateriel(newMateriel);
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public void updateMateriel(CMaterielModel majMateriel) throws SQLException {
		SCMaterielActivityLayer.updateMateriel(majMateriel);
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public void deleteMaterielByID(CContactModel deleteMaterielByID) throws SQLException {
		SCMaterielActivityLayer.deleteMaterielByID(deleteMaterielByID);
	}
}
