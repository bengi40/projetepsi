package com.cgi.udev.resoapi.web.model;

import java.util.List;

public class CMaterielModel {

	private int miId;
	private String msLibelle;
	private String msNumSerie;
	private boolean mbIsActived;
	private int miIdTypeMateriel;
	private String msTypeLibelle;
	private int miIdClient;
	private List<CInterfaceModel> mvInterface;

	public CMaterielModel() {
		this.miId = 0;
		this.msLibelle = "";
		this.msNumSerie = "";
		this.mbIsActived = true;
		this.miIdTypeMateriel = 0;
		this.miIdClient = 0;
	}
	
	public CMaterielModel(int piId, String psLibelle, String psNumSerie, boolean pbIsActived, int piIdTypeMateriel, int piIdClient) {
		this.miId = piId;
		this.msLibelle = psLibelle;
		this.msNumSerie = psNumSerie;
		this.mbIsActived = pbIsActived;
		this.miIdTypeMateriel = piIdTypeMateriel;
		this.miIdClient = piIdClient;
	}
	
	public CMaterielModel(int piId, String psLibelle, String psNumSerie, boolean pbIsActived, int piIdTypeMateriel, String psTypeLibelle) {
		this.miId = piId;
		this.msLibelle = psLibelle;
		this.msNumSerie = psNumSerie;
		this.mbIsActived = pbIsActived;
		this.miIdTypeMateriel = piIdTypeMateriel;
		this.msTypeLibelle = psTypeLibelle;
	}
	
	public CMaterielModel(int piId, String psLibelle, String psNumString, boolean pbIsActived, int piIdTypeMateriel, String psTypeLibelle, List<CInterfaceModel> pvInterface ) {
		this.miId = piId;
		this.msLibelle = psLibelle;
		this.msNumSerie = psNumString;
		this.mbIsActived = pbIsActived;
		this.miIdTypeMateriel = piIdTypeMateriel;
		this.msTypeLibelle = psTypeLibelle;
		this.mvInterface = pvInterface;
	}

	public int getMiId() {
		return miId;
	}

	public void setMiId(int miId) {
		this.miId = miId;
	}

	public String getMsLibelle() {
		return msLibelle;
	}

	public void setMsLibelle(String msLibelle) {
		this.msLibelle = msLibelle;
	}

	public String getMsNumSerie() {
		return msNumSerie;
	}

	public void setMsNumSerie(String msNumSerie) {
		this.msNumSerie = msNumSerie;
	}

	public boolean isMbIsActived() {
		return mbIsActived;
	}

	public void setMbIsActived(boolean mbIsActived) {
		this.mbIsActived = mbIsActived;
	}

	public int getMiIdTypeMateriel() {
		return miIdTypeMateriel;
	}

	public void setMiIdTypeMateriel(int miIdTypeMateriel) {
		this.miIdTypeMateriel = miIdTypeMateriel;
	}

	public String getMsTypeLibelle() {
		return msTypeLibelle;
	}

	public void setMsTypeLibelle(String msTypeLibelle) {
		this.msTypeLibelle = msTypeLibelle;
	}

	public List<CInterfaceModel> getMvInterface() {
		return mvInterface;
	}

	public void setMvInterface(List<CInterfaceModel> mvInterface) {
		this.mvInterface = mvInterface;
	}

	public int getMiIdClient() {
		return miIdClient;
	}

	public void setMiIdClient(int miIdClient) {
		this.miIdClient = miIdClient;
	}

}
