package com.cgi.udev.resoapi.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.cgi.udev.resoapi.web.model.CContactModel;

public class CContactDao {

	public CContactModel getContactByClient(int id) throws SQLException {
		try (Connection c = ResoDataSource.getSingleton().getConnection();
				PreparedStatement pstmt = c.prepareStatement(
						"select " +
						" 	p.id pid, " +
						"	p.nom nom, " + 
						"	p.prenom prenom , " + 
						"	p.telephone tel," + 
						"	p.email email," + 
						"	p.isActived isActived," +
						"	f.id fid," +
						"	f.libelle flib " +
						"from " + 
						"	personne p " + 
						"left outer join appartient a on a.idpersonne = p.id " + 
						"left outer join fonction f on f.id= a.idfonction " + 
						"where p.id = ?;")) {
			pstmt.setInt(1, id);

			ResultSet rs = pstmt.executeQuery();

			CContactModel oReturnedValue = null;
			try {
				if (rs.next()) {
					oReturnedValue = new CContactModel(
							rs.getInt("pid"),
							rs.getString("nom"), 
							rs.getString("prenom"), 
							rs.getString("tel"),
							rs.getString("email"),
							rs.getBoolean("isActived"),
							rs.getString("flib"));
				}
				c.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}
			c.close();
			return oReturnedValue;
		}
	}
	
	public void createContact(CContactModel newContact) throws SQLException {
		int idClient = 0;
		int idFonction = 0;
		String request = 	"INSERT INTO "
							+ 	"personne(nom, prenom, telephone,email)" 
							+ "VALUES "
							+	"(?, ?, ?, ?);";
		try (Connection c = ResoDataSource.getSingleton().getConnection();
			PreparedStatement pstmt = c.prepareStatement(request)) {
			
			pstmt.setString(1, newContact.getMsNom());
			pstmt.setString(2, newContact.getMsPrenom());
			pstmt.setString(3, newContact.getMsTelephone());
			pstmt.setString(4, newContact.getMsEmail());
						
			pstmt.executeUpdate();
			
			idClient = newContact.getMiIdClient();
			idFonction = newContact.getMiIdFonction();
			
			c.close();
			
			createClientContact(idClient, idFonction);
		}catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	public int createClientContact(int idClient, int idFonction) throws SQLException {
		int idContact = getLastId();
		
		String request = "INSERT INTO appartient(idpersonne,idfonction,idclient)"
				+ "VALUES(?,?,?);";
		try (Connection c = ResoDataSource.getSingleton().getConnection();
				PreparedStatement pstmt = c.prepareStatement(request)) {
			pstmt.setInt(1, idContact);
			pstmt.setInt(2, idFonction);
			pstmt.setInt(3, idClient);
			
			pstmt.executeUpdate();
			c.close();
		}
		return 0;
	}
	
	public static int getLastId() throws SQLException {
		
		String request = "SELECT max(id) id FROM personne;";
		
		try (Connection c = ResoDataSource.getSingleton().getConnection();
			Statement stmt = c.createStatement()) {
			int returnedValue = 0;
			
			ResultSet rs = stmt.executeQuery(request);
			if (rs.next()) {
				returnedValue = rs.getInt("id");
			}
			
			c.close();
			return returnedValue;			
		}
	}
	
	public void updateContact(CContactModel majContact) throws SQLException {
		
		String request = ("UPDATE personne " + 
							"SET " + 
							"	nom = ?," + 
							"	prenom = ?," + 
							"	telephone = ?, " + 
							"	email = ? " + 
							"WHERE id = ?;" );
		try (Connection c = ResoDataSource.getSingleton().getConnection();
				PreparedStatement pstmt = c.prepareStatement(request)) {
			pstmt.setString(1, majContact.getMsNom());
			pstmt.setString(2, majContact.getMsPrenom());
			pstmt.setString(3, majContact.getMsTelephone());
			pstmt.setString(4, majContact.getMsEmail());
			pstmt.setInt(5, majContact.getMiId());
			
			pstmt.executeUpdate();
			
			c.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}		
	}
	
	public void deleteContactByID(CContactModel delContact) throws SQLException {
		String request = ("UPDATE personne "
				+ "SET "
				+ "isActived = ? "
				+ "WHERE id = ?;");
		
		try (Connection c = ResoDataSource.getSingleton().getConnection();
				PreparedStatement pstmt = c.prepareStatement(request)) {
			pstmt.setBoolean(1,delContact.isMbIsActived());
			pstmt.setInt(2,delContact.getMiId());
			
			pstmt.executeUpdate();
			c.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}	
	}
}
