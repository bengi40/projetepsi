package com.cgi.udev.resoapi.web.restcontroller;

import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.cgi.udev.resoapi.web.activitylayer.SCContactActivityLayer;
import com.cgi.udev.resoapi.web.model.CContactModel;

@Path("/contact")
public class CContactRestFullService {
	
	/**
	 * Service REST Detail client
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public CContactModel getContactByClient(@PathParam("id") Integer id) throws SQLException {
		return SCContactActivityLayer.getContactByClient(id);
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public void createContact(CContactModel newContact) throws SQLException {
		SCContactActivityLayer.createContact(newContact);
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public void updateContact(CContactModel majContact) throws SQLException {
		SCContactActivityLayer.updateContact(majContact);
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public void deleteContactByID(CContactModel delContact) throws SQLException {
		SCContactActivityLayer.deleteContactByID(delContact);
	}
	
}
