package com.cgi.udev.resoapi.web.restcontroller;

import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.cgi.udev.resoapi.web.activitylayer.SCAdresseIpActivityLayer;
import com.cgi.udev.resoapi.web.model.CAdresseIpModel;

@Path("/adresseip")
public class CAdresseIpRestFullService {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public CAdresseIpModel getAdresseById(@PathParam("id") Integer id) throws SQLException {
		return SCAdresseIpActivityLayer.getAdresseById(id);
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public void createAdresseIp(CAdresseIpModel newAdresseIp) throws SQLException {
		SCAdresseIpActivityLayer.createAdresseIp(newAdresseIp);
	}
	 
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public void updateAdresseIp(CAdresseIpModel majAdresseIp) throws SQLException {
		SCAdresseIpActivityLayer.updateAdresseIp(majAdresseIp);
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public void deleteAdresseIp(@PathParam("id") Integer id) throws SQLException {
		SCAdresseIpActivityLayer.deleteAdresseIp(id);
	}
}
