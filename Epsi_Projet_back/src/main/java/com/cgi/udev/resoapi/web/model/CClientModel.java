package com.cgi.udev.resoapi.web.model;

import java.util.List;

public class CClientModel {

	private int miId;
	private String msNom;
	private String msAdresse1;
	private String msAdresse2;
	private String msCodePostal;
	private String msVille;
	private boolean mbIsActived;
	private int miIdCpVille;
	private List<CContactModel> mvContact;
	
	public CClientModel() {
		this.miId = 0;
		this.msNom = "";
		this.msAdresse1 = "";
		this.msAdresse2 = "";
		this.miIdCpVille = 0;
		this.mbIsActived = true;
	}

	public CClientModel(int piId, String psNom, String psAdresse1, String psAdresse2, int piIdCpVille, boolean pbIsActived) {
		this.miId = piId;
		this.msNom = psNom;
		this.msAdresse1 = psAdresse1;
		this.msAdresse2 = psAdresse2;
		this.miIdCpVille = piIdCpVille;
		this.mbIsActived = pbIsActived;
	}

	public CClientModel(int piId, String psNom, String psAdresse1, String psAdresse2, boolean pbIsActived, int piIdCpVille, String psCodePostal, String psVille) {
		this.miId = piId;
		this.msNom = psNom;
		this.msAdresse1 = psAdresse1;
		this.msAdresse2 = psAdresse2;
		this.mbIsActived = pbIsActived;
		this.miIdCpVille = piIdCpVille;
		this.msCodePostal = psCodePostal;
		this.msVille = psVille;
	}
	
	public CClientModel(int piId, String psNom, String psAdresse1, String psAdresse2, boolean pbIsActived, String psCodePostal, String psVille, List<CContactModel> pvContact) {
		this.miId = piId;
		this.msNom = psNom;
		this.msAdresse1 = psAdresse1;
		this.msAdresse2 = psAdresse2;
		this.mbIsActived = pbIsActived;
		this.msCodePostal = psCodePostal;
		this.msVille = psVille;
		this.mvContact = pvContact;
	}

	public int getMiId() {
		return miId;
	}

	public void setMiId(int miId) {
		this.miId = miId;
	}

	public String getMsNom() {
		return msNom;
	}

	public void setMsNom(String msNom) {
		this.msNom = msNom;
	}

	public String getMsAdresse1() {
		return msAdresse1;
	}

	public void setMsAdresse1(String msAdresse1) {
		this.msAdresse1 = msAdresse1;
	}

	public String getMsAdresse2() {
		return msAdresse2;
	}

	public void setMsAdresse2(String msAdresse2) {
		this.msAdresse2 = msAdresse2;
	}

	public String getMsCodePostal() {
		return msCodePostal;
	}

	public void setMsCodePostal(String msCodePostal) {
		this.msCodePostal = msCodePostal;
	}

	public String getMsVille() {
		return msVille;
	}

	public void setMsVille(String msVille) {
		this.msVille = msVille;
	}
	
	public List<CContactModel> getMvContact() {
		return mvContact;
	}

	public void setMvContact(List<CContactModel> mvContact) {
		this.mvContact = mvContact;
	}
	
	
	public int getMiIdCpVille() {
		return miIdCpVille;
	}

	public void setMiIdCpVille(int miIdCpVille) {
		this.miIdCpVille = miIdCpVille;
	}
	
	public boolean isMbIsActived() {
		return mbIsActived;
	}

	public void setMbIsActived(boolean mbIsActived) {
		this.mbIsActived = mbIsActived;
	}
}
