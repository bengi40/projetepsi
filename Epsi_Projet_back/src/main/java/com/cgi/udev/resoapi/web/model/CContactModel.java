package com.cgi.udev.resoapi.web.model;

public class CContactModel {

	private int miId;
	private String msNom;
	private String msPrenom;
	private String msTelephone;
	private String msEmail;
	private boolean mbIsActived;
	private String msFonction;
	private int miIdClient;
	private int miIdFonction;
	
	public CContactModel() {
		this.miId = 0;
		this.msNom = "";
		this.msPrenom = "";
		this.msTelephone = "";
		this.msEmail = "";
		this.mbIsActived = true;
	}
	
	public CContactModel(int piId, String psNom, String psPrenom, String psTelephone, String psEmail) {
		this.miId = piId;
		this.msNom = psNom;
		this.msPrenom = psPrenom;
		this.msTelephone = psTelephone;
		this.msEmail = psEmail;
	}
	
	public CContactModel(int piId, String psNom, String psPrenom, String psTelephone, String psEmail, boolean pbIsActived, String psFonction) {
		this.miId = piId;
		this.msNom = psNom;
		this.msPrenom = psPrenom;
		this.msTelephone = psTelephone;
		this.msEmail = psEmail;
		this.mbIsActived = pbIsActived;
		this.msFonction = psFonction;
	}
	
	public CContactModel(int piId, String psNom, String psPrenom, String psTelephone, String psEmail,boolean pbIsActived,int piIdClient, int piIdFonction) {
		this.miId = piId;
		this.msNom = psNom;
		this.msPrenom = psPrenom;
		this.msTelephone = psTelephone;
		this.msEmail = psEmail;
		this.mbIsActived = pbIsActived;
		this.miIdClient = piIdClient;
		this.miIdFonction = piIdFonction;
	}

	public int getMiId() {
		return miId;
	}

	public void setMiId(int miId) {
		this.miId = miId;
	}

	public String getMsNom() {
		return msNom;
	}

	public void setMsNom(String msNom) {
		this.msNom = msNom;
	}

	public String getMsTelephone() {
		return msTelephone;
	}

	public void setMsTelephone(String msTelephone) {
		this.msTelephone = msTelephone;
	}

	public String getMsEmail() {
		return msEmail;
	}

	public void setMsEmail(String msEmail) {
		this.msEmail = msEmail;
	}

	public String getMsFonction() {
		return msFonction;
	}

	public void setMsFonction(String msFonction) {
		this.msFonction = msFonction;
	}

	public String getMsPrenom() {
		return msPrenom;
	}

	public void setMsPrenom(String msPrenom) {
		this.msPrenom = msPrenom;
	}

	public boolean isMbIsActived() {
		return mbIsActived;
	}

	public void setMbIsActived(boolean mbIsActived) {
		this.mbIsActived = mbIsActived;
	}

	public int getMiIdClient() {
		return miIdClient;
	}

	public void setMiIdClient(int miIdClient) {
		this.miIdClient = miIdClient;
	}

	public int getMiIdFonction() {
		return miIdFonction;
	}

	public void setMiIdFonction(int miIdFonction) {
		this.miIdFonction = miIdFonction;
	}
		
}
