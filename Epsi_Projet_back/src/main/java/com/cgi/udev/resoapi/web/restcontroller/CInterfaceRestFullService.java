package com.cgi.udev.resoapi.web.restcontroller;

import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.cgi.udev.resoapi.web.activitylayer.SCInterfaceActivityLayer;
import com.cgi.udev.resoapi.web.model.CInterfaceModel;

@Path("/interface")
public class CInterfaceRestFullService {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public CInterfaceModel getInterfaceById(@PathParam("id") Integer id) throws SQLException {
		return SCInterfaceActivityLayer.getInterfaceById(id);
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public void createInterface(CInterfaceModel newInterface) throws SQLException {
		SCInterfaceActivityLayer.createInterface(newInterface);
	} 
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public void updateInterface(CInterfaceModel majInterface) throws SQLException {
		SCInterfaceActivityLayer.updateInterface(majInterface);
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public void deleteInterface(CInterfaceModel deleteInterface) throws SQLException {
		SCInterfaceActivityLayer.deleteInterface(deleteInterface);
	}
}
