package com.cgi.udev.resoapi.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.cgi.udev.resoapi.web.model.CClientModel;
import com.cgi.udev.resoapi.web.model.CContactModel;

public class CClientDao {

	public List<CClientModel> getClient() throws SQLException {
		
		String request = "select " 
								+ "client.id ci, " 
								+ "nom, " 
								+ "adresse1 ad1, " 
								+ "adresse2 ad2, "
								+ "isActived, "
								+ "ville.id vid,"
								+ "codepostal cp, "
								+ "ville " 
						+ "from client " 
						+ "left outer join ville on ville.id=client.id "
						+ "WHERE isActived = true;";
		
		try (Connection c = ResoDataSource.getSingleton().getConnection(); 
				Statement stmt = c.createStatement()) {
			try (ResultSet rs = stmt.executeQuery(request)) {

				List<CClientModel> vReturnedValue = new ArrayList<>();
				try {
					while (rs.next()) {
						vReturnedValue.add(new CClientModel(
								rs.getInt("ci"), 
								rs.getString("nom"), 
								rs.getString("ad1"),
								rs.getString("ad2"),
								rs.getBoolean("isActived"),
								rs.getInt("vid"),
								rs.getString("cp"), 
								rs.getString("ville")));
					}
					c.close();

				} catch (SQLException e) {
					e.printStackTrace();
				}
				return vReturnedValue;
			}
		}
	}

	public CClientModel getClientById(int id) throws SQLException {
		
		String request = "select 	" 
								+ "		client.id cid, "
								+ "		client.nom cnom, " 
								+ "		client.adresse1 cad, " 
								+ "		client.adresse2 cad2, "
								+ "		client.isActived isActived, "
								+ "		ville.codepostal cp, " 
								+ "		ville.ville ville, " 
								+ "		personne.id pid, "
								+ "		personne.nom pnom, " 
								+ "		personne.prenom ppre, "
								+ "		personne.telephone ptel, " 
								+ "		personne.email pemail, "
								+ "		personne.isActived isActived, "
								+ "		fonction.libelle flib " 
						+ "from client "
						+ "left outer join ville on ville.id = client.idcpville "
						+ "left outer join appartient on appartient.idclient = client.id "
						+ "left outer join personne on personne.id = appartient.idpersonne "
						+ "left outer join fonction on fonction.id = appartient.idfonction "
						+ "where client.id = ?;";
		
		try (Connection c = ResoDataSource.getSingleton().getConnection();
				PreparedStatement pstmt = c.prepareStatement(request)) {
			pstmt.setInt(1, id);

			ResultSet rs = pstmt.executeQuery();

			List<CContactModel> vContact = new ArrayList<>();
			CClientModel oReturnedValue = null;
			try {
				if (rs.next()) {
					oReturnedValue = new CClientModel(
							rs.getInt("cid"), 
							rs.getString("cnom"), 
							rs.getString("cad"),
							rs.getString("cad2"),
							rs.getBoolean("isActived"),
							rs.getString("cp"), 
							rs.getString("ville"), 
							vContact);
					do {
						if (rs.getInt("pid") != 0) {
							vContact.add(new CContactModel(
									rs.getInt("pid"), 
									rs.getString("pnom"), 
									rs.getString("ppre"),
									rs.getString("ptel"), 
									rs.getString("pemail"),
									rs.getBoolean("isActived"),
									rs.getString("flib")));
						}
					} while (rs.next());
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}
			c.close();
			return oReturnedValue;
		}
	}
	
	public void createClient(CClientModel newClient) throws SQLException {	
		String request = 	"INSERT INTO "
							+ 	"client(id, nom, adresse1, adresse2, idcpville)" 
							+ "VALUES "
							+	"(?, ?, ?, ?, ?);";
		try (Connection c = ResoDataSource.getSingleton().getConnection();
			PreparedStatement pstmt = c.prepareStatement(request)) {
				
			pstmt.setInt(1, newClient.getMiId());
			pstmt.setString(2, newClient.getMsNom());
			pstmt.setString(3, newClient.getMsAdresse1());
			pstmt.setString(4, newClient.getMsAdresse2());
			pstmt.setInt(5, newClient.getMiIdCpVille());
			
			pstmt.executeUpdate();
			c.close();
			
		}catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void updateClient(CClientModel majClient) throws SQLException {
		String request = 	"UPDATE "
							+"	client "
							+"SET "
							+"	nom = ?, " 
							+"	adresse1 = ?," 
							+"	adresse2 = ?, "
							+"	idcpville = ?, "
							+"	isActived = ? "
							+"WHERE " 
							+"	id = ?;";

		try (Connection c = ResoDataSource.getSingleton().getConnection();
				PreparedStatement pstmt = c.prepareStatement(request)) {
				
			pstmt.setString(1, majClient.getMsNom());
			pstmt.setString(2, majClient.getMsAdresse1());
			pstmt.setString(3, majClient.getMsAdresse2());
			pstmt.setInt(4, majClient.getMiIdCpVille());
			pstmt.setBoolean(5, majClient.isMbIsActived());
			pstmt.setInt(6, majClient.getMiId());
			
			pstmt.executeUpdate();
			c.close();
			
		}catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void delClientById(CClientModel delClient) throws SQLException {
		boolean isActived = true;
		int idClient = 0;
		
		String request = "UPDATE "
						+	"	client "
						+ "SET "
						+	"	isActived = ? "
						+ "WHERE " 
						+	"	id = ?; ";
						;
		try (Connection c = ResoDataSource.getSingleton().getConnection();
				PreparedStatement pstmt = c.prepareStatement(request);) {
				
			pstmt.setBoolean(1, delClient.isMbIsActived());
			pstmt.setInt(2, delClient.getMiId());
			
			isActived = delClient.isMbIsActived();
			idClient = delClient.getMiId();
			
			pstmt.executeUpdate();
			
			if(idClient != 0 && !isActived) {
				delClientContact(isActived, idClient);
			}
			
			c.close();
		}catch (SQLException e) {
			e.printStackTrace();
		}	
	}
	
	public void delClientContact(boolean isValided, int idClient) throws SQLException {
		String request = ("UPDATE "
				+	"	appartient "
				+ "SET "
				+	"	isActived = ? "
				+ "WHERE "
				+	"	idclient = ?;");
		
		try (Connection c = ResoDataSource.getSingleton().getConnection();
				PreparedStatement pstmt = c.prepareStatement(request);) {
				
			pstmt.setBoolean(1, isValided);
			pstmt.setInt(2, idClient);
			
			if(isValided && idClient != 0) {
				pstmt.executeUpdate();				
			}
			c.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}	
	}

}
