package com.cgi.udev.resoapi.web.activitylayer;

import java.sql.SQLException;

import com.cgi.udev.resoapi.dao.CInterfaceDao;
import com.cgi.udev.resoapi.web.model.CInterfaceModel;

public class SCInterfaceActivityLayer {

	public static CInterfaceModel getInterfaceById(int id) throws SQLException {
		CInterfaceDao oGetInterfaceById = new CInterfaceDao();
		return oGetInterfaceById.getInterfaceById(id);
	}

	public static void createInterface(CInterfaceModel newInterface) {
		CInterfaceDao oCreateInterface = new CInterfaceDao();
		oCreateInterface.createInterface(newInterface);
	}

	public static void updateInterface(CInterfaceModel majInterface) {
		CInterfaceDao oUpdateInterface = new CInterfaceDao();
		oUpdateInterface.updateInterface(majInterface);
	}

	public static void deleteInterface(CInterfaceModel deleteInterface) {
		CInterfaceDao oDeleteInterface = new CInterfaceDao();
		oDeleteInterface.deleteInterface(deleteInterface);
		
	}
}
