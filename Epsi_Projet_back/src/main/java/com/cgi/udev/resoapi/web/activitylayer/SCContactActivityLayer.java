package com.cgi.udev.resoapi.web.activitylayer;

import java.sql.SQLException;

import com.cgi.udev.resoapi.dao.CContactDao;
import com.cgi.udev.resoapi.web.model.CContactModel;

public class SCContactActivityLayer {
	
	/**
	 * Récupère les détails client par son ID
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public static CContactModel getContactByClient(int id) throws SQLException {
		CContactDao oContactDao = new CContactDao();
		return oContactDao.getContactByClient(id);
	}
	
	/**
	 * Création d'un nouveau contact
	 * @param newContact
	 * @param id : ID du client
	 * @throws SQLException
	 */
	public static void createContact(CContactModel newContact)  throws SQLException {
		CContactDao oContactDao = new CContactDao();
		oContactDao.createContact(newContact);
	}
	
	public static void updateContact(CContactModel majContact) throws SQLException {
		CContactDao oContactDao = new CContactDao();
		oContactDao.updateContact(majContact);
	}
	
	public static void deleteContactByID(CContactModel delContact) throws SQLException {
		CContactDao oContactDao = new CContactDao();
		oContactDao.deleteContactByID(delContact);
	}
	
}
