package com.cgi.udev.resoapi.web.activitylayer;

import java.sql.SQLException;
import java.util.List;

import com.cgi.udev.resoapi.dao.CClientDao;
import com.cgi.udev.resoapi.web.model.CClientModel;

public class SCClientActivityLayer {

	public static List<CClientModel> getClient() throws SQLException {
		CClientDao oClientDao = new CClientDao();
		return oClientDao.getClient();
	}
	
	public static CClientModel getClientById(int id) throws SQLException {
		CClientDao oClientDao = new CClientDao();
		return oClientDao.getClientById(id);
	}
	
	public static void createClient(CClientModel newClient) throws SQLException {
		CClientDao oClientDao = new CClientDao();
		oClientDao.createClient(newClient);
	}
	
	public static void updateClient(CClientModel majClient)throws SQLException {
		CClientDao oClientDao = new CClientDao();
		oClientDao.updateClient(majClient);
	}

	public static void delClientById(CClientModel delClient)throws SQLException {
		CClientDao oClientDao = new CClientDao();
		oClientDao.delClientById(delClient);
	}
}
