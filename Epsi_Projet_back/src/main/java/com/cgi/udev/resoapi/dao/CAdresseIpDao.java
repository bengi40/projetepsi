package com.cgi.udev.resoapi.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.ws.rs.PathParam;

import com.cgi.udev.resoapi.web.model.CAdresseIpModel;

public class CAdresseIpDao {

	public CAdresseIpModel getAdresseById(int id) throws SQLException {
		String request = "select " + 
				"	a.id aid," + 
				"	a.ipV4 ipv4," + 
				"	a.ipV6 ipv6," + 
				"	a.masque masque," + 
				"	a.isActived isActiv," + 
				"	ta.id taid," + 
				"	ta.libelle talib," + 
				"	i.id iid " + 
				"from " + 
				"	adresseip a " + 
				"left outer join typeaffectation ta on ta.id = a.id " + 
				"left outer join interface i on i.id = a.id;";

		try (Connection c = ResoDataSource.getSingleton().getConnection(); Statement stmt = c.createStatement()) {
			try (ResultSet rs = stmt.executeQuery(request)) {

				CAdresseIpModel oReturnedValue = null;
				try {
					if (rs.next()) {
						oReturnedValue = new CAdresseIpModel(
								rs.getInt("aid"), 
								rs.getString("ipv4"), 
								rs.getString("ipv6"),
								rs.getString("masque"), 
								rs.getBoolean("isActiv"), 
								rs.getInt("taid"), 
								rs.getString("talib"),
								rs.getInt("iid"));
					}
					c.close();

				} catch (SQLException e) {
					e.printStackTrace();
				}
				return oReturnedValue;
			}
		}
	}

	public void createAdresseIp(CAdresseIpModel newAdresseIp) {
		String request = "INSERT INTO adresseip " + 
							"	(ipV4, ipV6,masque,isActived,idinterface,idtypeaff) " + 
							"VALUES " + 
							"	(?,?,?,?,?,?);";
				
		try (Connection c = ResoDataSource.getSingleton().getConnection();
				PreparedStatement pstmt = c.prepareStatement(request)) {
			
			pstmt.setString(1,newAdresseIp.getMsIpv4());	
			pstmt.setString(2, newAdresseIp.getMsIpv6());
			pstmt.setString(3, newAdresseIp.getMsMasque());
			pstmt.setBoolean(4, newAdresseIp.isMbIsActived());
			pstmt.setInt(5,newAdresseIp.getMiInterfaceId());
			pstmt.setInt(6, newAdresseIp.getMiIdType());
			
			pstmt.executeUpdate();
			c.close();
			
		}catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void updateAdresseIp(CAdresseIpModel majAdresseIp) throws SQLException {
		String request = "UPDATE " +
							"adresseip " + 
						"SET	" + 
						"	ipV4 = ?, " + 
						"	ipV6 = ?, " +
						"	masque = ?, " +
						"	isActived = ?, " +
						"	idinterface = ?, " +
						"	idtypeaff = ? " +
						"WHERE " + 
						"	id = ?;";
		
		try (Connection c = ResoDataSource.getSingleton().getConnection();
				PreparedStatement pstmt = c.prepareStatement(request)) {
			
			pstmt.setString(1, majAdresseIp.getMsIpv4());
			pstmt.setString(2, majAdresseIp.getMsIpv6());
			pstmt.setString(3, majAdresseIp.getMsMasque());
			pstmt.setBoolean(4, majAdresseIp.isMbIsActived());
			pstmt.setInt(5, majAdresseIp.getMiInterfaceId());
			pstmt.setInt(6, majAdresseIp.getMiIdType());
			pstmt.setInt(7, majAdresseIp.getMiId());
			
			pstmt.executeUpdate();
			
			c.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}		
		
	}
	
	public void deleteAdresseIp(int id) throws SQLException {
		String request = "update adresseip " + 
							"set	" + 
							"	isActived = ?, " +
							"where " + 
							"	id = ?;";
		
		try (Connection c = ResoDataSource.getSingleton().getConnection();
				PreparedStatement pstmt = c.prepareStatement(request)) {
			
			pstmt.setBoolean(1, deleteAdresseIp.isMbIsActived());
			pstmt.setInt(2, deleteAdresseIp.getMiId());
			
			pstmt.executeUpdate();
			
			c.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}		
		
	}
}
