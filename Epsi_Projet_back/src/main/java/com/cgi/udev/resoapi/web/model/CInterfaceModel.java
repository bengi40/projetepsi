package com.cgi.udev.resoapi.web.model;

import java.util.List;

public class CInterfaceModel {

	private int miId;
	private String msNom;
	private String msMac;
	private boolean mbIsactivd;
	private List<CAdresseIpModel> mvAdresseIp;
	private int miIdType;
	private int miIdMateriel;
	
	public CInterfaceModel() {
		this.miId = 0;
		this.msNom = "";
		this.msMac = "";
		this.mbIsactivd = true;
		this.miIdType = 0;
		this.miIdMateriel = 0;
	}
	
	public CInterfaceModel (int piId, String psNom, String psMac, boolean pbIsactived, List<CAdresseIpModel> pvAdresseIp) {
		this.miId = piId;
		this.msNom = psNom;
		this.msMac = psMac;
		this.mbIsactivd = pbIsactived;
		this.mvAdresseIp = pvAdresseIp;
	}

	public int getMiId() {
		return miId;
	}

	public void setMiId(int miId) {
		this.miId = miId;
	}

	public String getMsNom() {
		return msNom;
	}

	public void setMsNom(String msNom) {
		this.msNom = msNom;
	}

	public String getMsMac() {
		return msMac;
	}

	public void setMsMac(String msMac) {
		this.msMac = msMac;
	}

	public boolean isMbIsactivd() {
		return mbIsactivd;
	}

	public void setMbIsactivd(boolean mbIsactivd) {
		this.mbIsactivd = mbIsactivd;
	}

	public List<CAdresseIpModel> getMvAdresseIp() {
		return mvAdresseIp;
	}

	public void setMvAdresseIp(List<CAdresseIpModel> mvAdresseIp) {
		this.mvAdresseIp = mvAdresseIp;
	}

	public int getMiIdType() {
		return miIdType;
	}

	public void setMiIdType(int miIdType) {
		this.miIdType = miIdType;
	}

	public int getMiIdMateriel() {
		return miIdMateriel;
	}

	public void setMiIdMateriel(int miIdMateriel) {
		this.miIdMateriel = miIdMateriel;
	}
	
}
