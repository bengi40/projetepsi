package com.cgi.udev.resoapi.web.activitylayer;

import java.sql.SQLException;

import com.cgi.udev.resoapi.dao.CMaterielDao;
import com.cgi.udev.resoapi.web.model.CContactModel;
import com.cgi.udev.resoapi.web.model.CMaterielModel;

public class SCMaterielActivityLayer {

	public static CMaterielModel getMaterielById(Integer id) throws SQLException {
		CMaterielDao oGetMaterielById = new CMaterielDao();
		return oGetMaterielById.getMaterielById(id); 
	}

	public static void createMateriel(CMaterielModel newMateriel) throws SQLException {
		CMaterielDao oCreateMateriel = new CMaterielDao();
		oCreateMateriel.createMateriel(newMateriel);
	}

	public static void updateMateriel(CMaterielModel updateMateriel) throws SQLException {
		CMaterielDao oUpdateMateriel = new CMaterielDao();
		oUpdateMateriel.updateMateriel(updateMateriel);
	}

	public static void deleteMaterielByID(CContactModel deleteMaterielByID) throws SQLException {
		CMaterielDao oDeleteMaterielByID = new CMaterielDao();
		oDeleteMaterielByID.deleteMaterielByID(deleteMaterielByID);
	}

}
