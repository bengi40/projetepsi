import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

/**
 * Import des pages
 */
import {
	ClientsComponent,
	DetailClientComponent,
	CreationClientComponent
} from './pages/pages';

/**
 * Import des Components
 */
import {
	NavbarComponent
} from './components/components';

/**
 * Import des services
 */
import {
	ClientService,
	UtilsService
} from './providers/providers';

import {
	MatInputModule,
	MatCardModule,
	MatTableModule,
	MatDividerModule,
	MatIconModule,
	MatToolbarModule,
	MatMenuModule,
	MatButtonModule,
	MatFormFieldModule
} from '@angular/material';

@NgModule({
	declarations: [
		AppComponent,
		ClientsComponent,
		DetailClientComponent,
		NavbarComponent,
		CreationClientComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		HttpClientModule,
		FlexLayoutModule,
		MatInputModule,
		MatCardModule,
		MatTableModule,
		BrowserAnimationsModule,
		MatDividerModule,
		MatIconModule,
		MatToolbarModule,
		MatMenuModule,
		MatButtonModule,
		MatFormFieldModule,
		FormsModule,
		ReactiveFormsModule
	],
	providers: [
		ClientService,
		UtilsService
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
