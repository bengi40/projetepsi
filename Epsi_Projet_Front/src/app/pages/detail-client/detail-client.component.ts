import { Component } from '@angular/core';
import { ClientService, UtilsService } from 'src/app/providers/providers';
import { Client } from 'src/app/models/client';
import { MatTableDataSource } from '@angular/material';

@Component({
	selector: 'detail-client',
	templateUrl: './detail-client.component.html',
	styleUrls: ['./detail-client.component.css']
})
export class DetailClientComponent {

	client: Client;

	/**
	 * Tableau
	 */
	displayedColumns: string[] = ['nom', 'prenom', 'telephone', 'email', 'fonction', 'action'];
	dataSource = new MatTableDataSource();


	/**
	 * Constructeur
	 */
	constructor(clientService: ClientService, private readonly utilsService: UtilsService) {
		const param = this.utilsService.getParamUrl('miId');
		if (param && param.length) {
			clientService.getClientById(param[0]).then(client => {
				this.client = client;
				this.dataSource.data = client.mvContact;
			});
		}
	}

}
