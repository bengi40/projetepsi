import { Component, OnInit } from '@angular/core';
import { ClientService, UtilsService, RoutesService } from 'src/app/providers/providers';
import { Client } from 'src/app/models/client';
import { MatTableDataSource } from '@angular/material';

@Component({
	selector: 'clients',
	templateUrl: './clients.component.html',
	styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit {

	creationclient = RoutesService.CREATIONCLIENT;

	/**
	 * Initialisation de la liste des Spés
	 */
	mvListeClient: Array<Client>;

	/**
	 * Constructeur
	 */
	constructor(clientService: ClientService, private readonly utilsService: UtilsService) {
		clientService.getListe().then(listeClient => this.dataSource.data = listeClient);
	}

	/**
	 * Tableau
	 */
	displayedColumns: string[] = ['nom', 'code postal', 'ville', 'supprimer'];
	dataSource = new MatTableDataSource();

	ngOnInit() {
		this.dataSource.data = this.mvListeClient;
	}

	route(piId: number) {
		this.utilsService.navigate(RoutesService.CLIENT, { miId: piId });
	}

	routeGlobal(psChemin: string) {
		this.utilsService.navigate(psChemin);
	}

}
