import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientsComponent } from './clients.component';
import { ClientService } from 'src/app/providers/providers';

describe('ClientsComponent', () => {
	let component: ClientsComponent;
	let fixture: ComponentFixture<ClientsComponent>;

	const clientServiceStub = {
		getListe: () => Promise.resolve([])
	};

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ClientsComponent],
			providers: [
				{ provide: ClientService, useValue: clientServiceStub }
			]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ClientsComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
