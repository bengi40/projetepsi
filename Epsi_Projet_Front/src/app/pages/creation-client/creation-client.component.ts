import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

@Component({
	selector: 'app-creation-client',
	templateUrl: './creation-client.component.html',
	styleUrls: ['./creation-client.component.css']
})
export class CreationClientComponent implements OnInit {

	creationClient: FormGroup;
	submitted = false;

	constructor(private formBuilder: FormBuilder) { }

	ngOnInit() {
		this.creationClient = this.formBuilder.group({
			nom: ['', Validators.required],
			adresse1: ['', Validators.required],
			adresse2: ['', Validators.required],
			cp: ['', Validators.required],
			ville: ['', Validators.required]
		});
	}

	get f() { return this.creationClient.controls; }

	onSubmit() {
		this.submitted = true;

		if (this.creationClient.invalid) {
			return;
		}

		alert('Le client a été enregistré avec succès.');
	}

}
