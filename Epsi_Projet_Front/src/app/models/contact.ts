export class Contact {

	private _miId: number;

	private _msNom: string;

	private _msPrenom: string;

	private _msTelephone: string;

	private _msEmail: string;

	private _msFonction: string;

	/**
	 * Constructeur du client
	 */
	constructor(jsonFromBack?: Contact) {
		if (jsonFromBack) {
			this.miId = jsonFromBack.miId;
			this.msNom = jsonFromBack.msNom;
			this.msPrenom = jsonFromBack.msPrenom;
			this.msTelephone = jsonFromBack.msTelephone;
			this.msEmail = jsonFromBack.msEmail;
			this.msFonction = jsonFromBack.msFonction;
		}
	}

	/**
	 * Getter miId
	 */
	public get miId(): number {
		return this._miId;
	}

	/**
	 * Setter miId
	 */
	public set miId(value: number) {
		this._miId = value;
	}

	/**
	 * Getter msNom
	 */
	public get msNom(): string {
		return this._msNom;
	}

	/**
	 * Setter msNom
	 */
	public set msNom(value: string) {
		this._msNom = value;
	}

	/**
	 * Getter msPrenom
	 */
	public get msPrenom(): string {
		return this._msPrenom;
	}

	/**
	 * Setter msPrenom
	 */
	public set msPrenom(value: string) {
		this._msPrenom = value;
	}

	/**
	 * Getter msTel
	 */
	public get msTelephone(): string {
		return this._msTelephone;
	}

	/**
	 * Setter msTel
	 */
	public set msTelephone(value: string) {
		this._msTelephone = value;
	}

	/**
	 * Getter msMail
	 */
	public get msEmail(): string {
		return this._msEmail;
	}

	/**
	 * Setter msMail
	 */
	public set msEmail(value: string) {
		this._msEmail = value;
	}

	/**
	 * Getter msFonction
	 */
	public get msFonction(): string {
		return this._msFonction;
	}

	/**
	 * Setter msFonction
	 */
	public set msFonction(value: string) {
		this._msFonction = value;
	}

}
