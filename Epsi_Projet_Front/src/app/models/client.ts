import { Contact } from './contact';

export class Client {

	/**
	 * Id d'un client
	 */
	private _miId: number;

	/**
	 * Nom d'un client
	 */
	private _msNom: string;

	/**
	 * Adresse client
	 */
	private _msAdresse1: string;

	/**
	 * Adresse client supplémentaire
	 */
	private _msAdresse2: string;

	/**
	 * Cpde postal client
	 */
	private _msCodePostal: string;

	/**
	 * ville client
	 */
	private _msVille: string;

	/**
	 * Activer client
	 */
	private _mbIsActived: boolean;

	/**
	 * liste contact
	 */
	private _mvContact: Array<Contact>;

	/**
	 * Constructeur du client
	 */
	constructor(jsonFromBack?: Client) {
		this.mvContact = [];
		if (jsonFromBack) {
			this.miId = jsonFromBack.miId;
			this.msNom = jsonFromBack.msNom;
			this.msAdresse1 = jsonFromBack.msAdresse1;
			this.msCodePostal = jsonFromBack.msCodePostal;
			this.mbIsActived = jsonFromBack.mbIsActived;
			this.msVille = jsonFromBack.msVille;
			if (jsonFromBack.mvContact && jsonFromBack.mvContact.length) {
				jsonFromBack.mvContact.forEach(contact => this.mvContact.push(new Contact(contact)));
			}
		}
	}

	/**
	 * Getter miId
	 */
	public get miId(): number {
		return this._miId;
	}

	/**
	 * Setter miId
	 */
	public set miId(value: number) {
		this._miId = value;
	}

	/**
	 * Getter msNom
	 */
	public get msNom(): string {
		return this._msNom;
	}

	/**
	 * Setter msNom
	 */
	public set msNom(value: string) {
		this._msNom = value;
	}

	/**
	 * Getter msAdresse1
	 */
	public get msAdresse1(): string {
		return this._msAdresse1;
	}

	/**
	 * Setter msAdresse1
	 */
	public set msAdresse1(value: string) {
		this._msAdresse1 = value;
	}

	/**
	 * Getter msAdresse2
	 */
	public get msAdresse2(): string {
		return this._msAdresse2;
	}

	/**
	 * Setter msAdresse2
	 */
	public set msAdresse2(value: string) {
		this._msAdresse2 = value;
	}

	/**
	 * Getter msCodePostal
	 */
	public get msCodePostal(): string {
		return this._msCodePostal;
	}

	/**
	 * Setter msCodePostal
	 */
	public set msCodePostal(value: string) {
		this._msCodePostal = value;
	}

	/**
	 * Getter msVille
	 */
	public get msVille(): string {
		return this._msVille;
	}

	/**
	 * Setter msVille
	 */
	public set msVille(value: string) {
		this._msVille = value;
	}

	/**
	 * Getter mvContact
	 */
	public get mvContact(): Array<Contact> {
		return this._mvContact;
	}

	/**
	 * Setter mvContact
	 */
	public set mvContact(value: Array<Contact>) {
		this._mvContact = value;
	}

	/**
	 * Getter mbIsValid
	 */
	public get mbIsActived(): boolean {
		return this._mbIsActived;
	}

	/**
	 * Setter mbIsValid
	 */
	public set mbIsActived(value: boolean) {
		this._mbIsActived = value;
	}
}
