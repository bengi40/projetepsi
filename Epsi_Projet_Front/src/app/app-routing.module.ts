import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoutesService } from './providers/providers';

/**
 * Import des différentes pages
 */
import {
	ClientsComponent,
	DetailClientComponent,
	CreationClientComponent
} from './pages/pages';

const routes: Routes = [
	{
		path: '',
		redirectTo: RoutesService.CLIENTS,
		pathMatch: 'full',
	},
	{
		path: RoutesService.CLIENTS,
		pathMatch: 'full',
		component: ClientsComponent
	},
	{
		path: RoutesService.CLIENT,
		component: DetailClientComponent
	},
	{
		path: RoutesService.CREATIONCLIENT,
		pathMatch: 'full',
		component: CreationClientComponent
	}
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
