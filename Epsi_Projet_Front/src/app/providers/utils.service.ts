import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
	providedIn: 'root'
})
export class UtilsService {

	constructor(private readonly router: Router) { }

	private getUrlLikeRouter(): string {
		if (this.router && this.router.routerState && this.router.routerState.snapshot) {
			return this.router.routerState.snapshot.url;
		} else if (this.router) {
			return this.router.url;
		} else {
			return null;
		}
	}

	getParamUrl(psParamName: string): Array<string> {
		const valueOfParam = [];
		const tabUrl = this.getUrlLikeRouter().split('?');
		if (tabUrl.length > 1) {
			const tabParams = tabUrl[1].split('&');
			tabParams.forEach(param => {
				const keyVal = param.split('=');
				if (keyVal.length > 1 && keyVal[0] === psParamName) {
					valueOfParam.push(keyVal[1]);
				}
			});
		}
		return valueOfParam;
	}

	navigate(psUrl: string, poParam?: { [k: string]: string | number | Array<string | number> }) {
		this.router.navigate([psUrl], { skipLocationChange: false, queryParams: poParam });
	}
}
