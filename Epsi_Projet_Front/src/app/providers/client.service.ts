import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Client } from '../models/models';

export class ClientService {

	constructor(private readonly http: HttpClient) { }

	/**
	 * recup la liste des clients
	 */
	getListe(): Promise<Array<Client>> {
		return this.http.get<Array<Client>>(`${environment.urlBack}resoapi/api/client`).toPromise()
			.then(clients => {
				const clientsToReturned = [];
				if (clients && clients.length) {
					clients.forEach(client => clientsToReturned.push(new Client(client)));
				}
				return clientsToReturned;
			});
	}

	getClientById(piId: number | string): Promise<Client> {
		return this.http.get<Client>(`${environment.urlBack}resoapi/api/client/` + piId).toPromise()
			.then(clients => new Client(clients));
	}

}
