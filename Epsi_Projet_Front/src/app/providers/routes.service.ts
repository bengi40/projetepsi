

export class RoutesService {

	/**
	 * Routage de l'accueil
	 */
	// static ACCUEIL = 'accueil';

	/**
	 * Routage liste des clients
	 */
	static CLIENTS = 'clients';

	/**
	 * Routage d'un client
	 */
	static CLIENT = 'client';

	/**
	 * Routage de la création d'un client
	 */
	static CREATIONCLIENT = 'creation-client';

	/**
	 * Constructeur
	 */
	private constructor() { }
}
