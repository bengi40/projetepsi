import { Component } from '@angular/core';
import { RoutesService, UtilsService } from 'src/app/providers/providers';

@Component({
	selector: 'navbar',
	templateUrl: './navbar.component.html',
	styleUrls: ['./navbar.component.css']
})
export class NavbarComponent  {

	listeClient = RoutesService.CLIENTS;

	/**
	 * Constructeur
	 * @param utilsService Constructeur
	 */
	constructor(private readonly utilsService: UtilsService) { }

	route(psChemin: string) {
	this.utilsService.navigate(psChemin);
	}
}
